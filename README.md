# Cash Machine
[Cash Machine](https://gitlab.com/YAAK/cash-machine) is a very simple simulation of a cash machine process of delivering bank notes.

The goal is to show what is the lowest number of possible notes for a specific amount.
Available notes are 100, 50, 20 & 10.

It is not meant to be deployed in production.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You'll need [Docker Engine Community Edition](https://www.docker.com/products/docker-engine) and [Docker Compose](https://docs.docker.com/compose/) to be able to run this project on your local machine.

### Installing

Considering you have Docker Engine and Docker Compose installed an running on your Linux machine:

Get a copy of the project on your local machine:
```
git clone git@gitlab.com:YAAK/cash-machine.git
```
Run the project using Docker Composer

```
docker-composer up cash-machine
```

After container is successfully launched, open your browser and go to `http://localhost:5020` or use curl to make an API call:

```
curl --request POST \
  --url http://localhost:5020/account/withdraw \
  --header 'content-type: application/json' \
  --data '{"amount": 100}'
```

## API Endpoints

Please refer to [Technical Docs](docs/README.md) for a list of available endpoints

## Running the tests

Run the test using the container's shell.

Find the name of the container using below command:
```
docker-compose ps
```

Then get a shell in your container using below command:
```
docker exec -it NAME-OF-CONTAINER bash
```

To run the tests from the shell in the container, run the following command:
```
./vendor/bin/phpunit --testdox
```

## Built With

* [Lumen](https://lumen.laravel.com/docs/5.6) - The web framework used
* [PHPUnit](https://phpunit.readthedocs.io/en/7.3/) - The Test framework
* [Composer](https://getcomposer.org/doc/) - The Package Management tool

## Versioning

[SemVer](http://semver.org/) is used for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/YAAK/cash-machine/tags).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
