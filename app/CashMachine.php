<?php
declare(strict_types = 1);

namespace App;

use App\Exceptions\InvalidAmountException;
use App\Exceptions\NoteUnavailableException;
use Illuminate\Database\Eloquent\Model;

class CashMachine extends Model
{
    /**
     * Available Notes
     *
     * @var array
     */
    protected $noteSet = [
        100, 50, 20, 10
    ];

    public function __construct()
    {
        parent::__construct();
        // Sort Set of notes from highest to lowest
        rsort($this->noteSet);
    }

    /**
     * Withdraw an amount from an infinite deposit of money
     *
     * @param int|null $amount
     * @return array
     * @throws NoteUnavailableException
     */
    public function withdraw($amount = null): array
    {
        if (empty($amount)) {
            $amount = 0;
        }

        $this->validateAmount($amount);

        return $this->compileToNotes($amount);
    }

    /**
     * Returns the least possible number of notes that add up to the desired amount
     * 
     * @param $amount
     * @return array
     */
    private function compileToNotes($amount) {
        $minPossibleNotes = [];
        $rem = $amount;

        foreach ($this->noteSet as $note) {
            $numberOfThisNote = floor($rem / $note);
            if($numberOfThisNote > 0) {
                $minPossibleNotes[$note] = $numberOfThisNote;
            }
            $rem = $rem - $numberOfThisNote * $note;
        }

        return $minPossibleNotes;
    }
    
    private function validateAmount($amount): void
    {
        if ($amount < 0) {
            throw new InvalidAmountException();
        }

        if (floatval($amount) != intval($amount)) {
            throw new NoteUnavailableException();
        }

        $mod = 0;
        foreach ($this->noteSet as $note) {
            $mod = $amount % $note;
        }
        $isAnyNoteCombinationAvailableForAmount = ($mod === 0);

        if (!$isAnyNoteCombinationAvailableForAmount) {
            throw new NoteUnavailableException();
        }
    }
}
