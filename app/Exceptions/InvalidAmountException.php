<?php

namespace App\Exceptions;

class InvalidAmountException extends \InvalidArgumentException
{
    public function __construct()
    {
        return parent::__construct("Amount is not valid", 422);
    }
}