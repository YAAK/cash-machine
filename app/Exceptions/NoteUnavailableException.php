<?php

namespace App\Exceptions;

class NoteUnavailableException extends \Exception
{
    public function __construct()
    {
        return parent::__construct("Not enough variety of notes are available to fulfil the request", 422);
    }
}