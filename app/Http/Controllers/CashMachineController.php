<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\CashMachine;
use App\Exceptions\InvalidAmountException;
use App\Exceptions\NoteUnavailableException;
use Illuminate\Http\Request;

class CashMachineController extends Controller
{
    protected $cashMachine;

    /**
     * Create a new controller instance.
     *
     * @param CashMachine $cashMachine
     */
    public function __construct(CashMachine $cashMachine)
    {
        $this->cashMachine = $cashMachine;
    }

    /**
     * Withdraw an amount from an account
     *
     * @param Request $request
     * @return mixed
     */
    public function withdraw(Request $request)
    {
        $this->validate($request, [
            'amount' => 'nullable'
        ]);

        try {
            return response()->json($this->cashMachine->withdraw($request->get('amount', null)), 201);
        } catch (NoteUnavailableException $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        } catch (InvalidAmountException $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
