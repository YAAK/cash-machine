<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    // Display a simple form to facilitate testing :)

    return "<h2>Cash Machine </h2>\n" .
        "<p>Try this endpoint: POST /account/withdraw <br/>\n".
    "<form action='/account/withdraw' method='POST'>" .
    "<input type='text' value='100' name='amount'/>" .
    "<input type='submit'/></form>";
});

$router->post('/account/withdraw', ['uses' => 'CashMachineController@withdraw']);
