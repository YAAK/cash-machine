# Cash Machine Technical Documents - API Endpoints

## POST `/account/withdraw`
Create a withdraw transaction

### Request Body
| Field  | Required | Description                                            |
| :---   | :---     | :---                                                   |
| amount | yes      | The amount to be withdrawn. Must be a positive integer |

## Response by status

### [201 CREATED](https://httpstatuses.com/201)
The transaction was fulfilled and the response shows the number of delivered notes.

Sample response for `amount = 210`:

```json
{
    "100": 2,
    "10": 1
}
```

### [422 Unprocessable Entity](https://httpstatuses.com/422)
```json
{
    "message": "Not enough variety of notes are available to fulfil the request"
}
```

### [422 Unprocessable Entity](https://httpstatuses.com/422)
```json
{
    "message": "Amount is not valid"
}
```