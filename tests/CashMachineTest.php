<?php
declare(strict_types = 1);

use PHPUnit\Framework\TestResult;
use App\Exceptions\NoteUnavailableException;
use App\Exceptions\InvalidAmountException;
use App\CashMachine;

class CashMachineTest extends TestCase
{
    public function testAmount10(): void
    {
        $this->assertEquals(
            ["10" => 1],
            (new CashMachine())->withdraw(10),
            'Doesn\'t work for 10'
        );
    }

    public function testAmount30(): void
    {
        $this->assertEquals(
            ["20" => 1, "10" => 1],
            (new CashMachine())->withdraw(30),
            'Doesn\'t work for 30'
        );
    }

    public function testAmount50(): void
    {
        $this->assertEquals(
            ["50" => 1],
            (new CashMachine())->withdraw(50),
            'Doesn\'t work for 50'
        );
    }

    public function testAmount1750(): void
    {
        $this->assertEquals(
            ["100" => 17, "50" => 1],
            (new CashMachine())->withdraw(1750),
            'Doesn\'t work for 1750'
        );
    }

    public function testAmount180(): void
    {
        $this->assertEquals(
            ["100" => 1, "50" => 1, "20" => 1, "10" => 1],
            (new CashMachine())->withdraw(180),
            'Doesn\'t work for 180'
        );
    }

    public function testAmount190(): void
    {
        $this->assertEquals(
            ["100" => 1, "50" => 1, "20" => 2],
            (new CashMachine())->withdraw(190),
            'Doesn\'t work for 190'
        );
    }

    public function testAmount210(): void
    {
        $this->assertEquals(
            ["100" => 2, "10" => 1],
            (new CashMachine())->withdraw(210),
            'Doesn\'t work for 210'
        );
    }

    public function testNullAmount(): void
    {
        $this->assertEquals(
            [],
            (new CashMachine())->withdraw(null),
            'Doesn\'t work for null'
        );
    }

    public function testZeroAmount(): void
    {
        $this->assertEquals(
            [],
            (new CashMachine())->withdraw(0),
            'Doesn\'t work for 0'
        );
    }

    public function testThrowsExceptionFor325(): void
    {
        $this->expectException(NoteUnavailableException::class);

        (new CashMachine())->withdraw(325);
    }

    public function testThrowsExceptionFor100Point1(): void
    {
        $this->expectException(NoteUnavailableException::class);

        (new CashMachine())->withdraw(100.1);
    }

    public function testThrowsExceptionFor50Point9(): void
    {
        $this->expectException(NoteUnavailableException::class);

        (new CashMachine())->withdraw(50.9);
    }

    public function testThrowsExceptionForNegativeAmount(): void
    {
        $this->expectException(InvalidAmountException::class);

        (new CashMachine())->withdraw(-1);
    }

    /**
     * Runs a test and collects its result in a TestResult instance.
     * @param \PHPUnit\Framework\TestResult $result
     * @return \PHPUnit\Framework\TestResult
     */
    public function run(TestResult $result = null): TestResult
    {
        return parent::run($result);
    }
}