<?php
declare(strict_types = 1);

use PHPUnit\Framework\TestResult;

class CashMachineControllerTest extends TestCase
{
    public function testWithdraw10(): void
    {
        $this->post('/account/withdraw', ['amount' => 10])
            ->seeJsonEquals(["10" => 1])->assertResponseStatus(201);
    }

    public function testWithdraw20(): void
    {
        $this->post('/account/withdraw', ['amount' => 20])
            ->seeJsonEquals(["20" => 1])->assertResponseStatus(201);
    }

    public function testWithdraw30(): void
    {
        $this->post('/account/withdraw', ['amount' => 30])
            ->seeJsonEquals(["20" => 1, "10" => 1])->assertResponseStatus(201);
    }

    public function testAmount50(): void
    {
        $this->post('/account/withdraw', ['amount' => 50])
            ->seeJsonEquals(["50" => 1])->assertResponseStatus(201);
    }

    public function testAmount1750(): void
    {
        $this->post('/account/withdraw', ['amount' => 1750])
            ->seeJsonEquals(["100" => 17, "50" => 1])->assertResponseStatus(201);
    }

    public function testAmount180(): void
    {
        $this->post('/account/withdraw', ['amount' => 180])
            ->seeJsonEquals(["100" => 1, "50" => 1, "20" => 1, "10" => 1])->assertResponseStatus(201);
    }

    public function testAmount190(): void
    {
        $this->post('/account/withdraw', ['amount' => 190])
            ->seeJsonEquals(["100" => 1, "50" => 1, "20" => 2])->assertResponseStatus(201);
    }

    public function testAmount210(): void
    {
        $this->post('/account/withdraw', ['amount' => 210])
            ->seeJsonEquals(["100" => 2, "10" => 1])->assertResponseStatus(201);
    }

    public function testNullAmount(): void
    {
        $this->post('/account/withdraw', ['amount' => null])
            ->seeJsonEquals([])->assertResponseStatus(201);
    }

    public function testZeroAmount(): void
    {
        $this->post('/account/withdraw', ['amount' => 0])
            ->seeJsonEquals([])->assertResponseStatus(201);
    }

    public function testSeeErrorFor325(): void
    {
        $this->post('/account/withdraw', ['amount' => 325])
            ->seeJsonEquals(["error"=>"Not enough variety of notes are available to fulfil the request"])
            ->assertResponseStatus(422);
    }

    public function testSeeErrorFor100Point1(): void
    {
        $this->post('/account/withdraw', ['amount' => 100.1])
            ->seeJsonEquals(["error"=>"Not enough variety of notes are available to fulfil the request"])
            ->assertResponseStatus(422);
    }

    public function testSeeErrorFor50Point9(): void
    {
        $this->post('/account/withdraw', ['amount' => 50.9])
            ->seeJsonEquals(["error"=>"Not enough variety of notes are available to fulfil the request"])
            ->assertResponseStatus(422);
    }

    public function testSeeErrorForNegativeAmount(): void
    {
        $this->post('/account/withdraw', ['amount' => -100])
            ->seeJsonEquals(["error"=>"Amount is not valid"])->assertResponseStatus(422);
    }

    /**
     * Runs a test and collects its result in a TestResult instance.
     * @param \PHPUnit\Framework\TestResult $result
     * @return \PHPUnit\Framework\TestResult
     */
    public function run(TestResult $result = null): TestResult
    {
        return parent::run($result);
    }
}
